# Api-gateway на базе krakend

### Инструкция по развертыванию проекта:

* ``brew instal mkcert``  Выпуск сертификатов для macOS
* ``mkcert --install``
* В корневой директории проекта _cd docker/nginx/ssl/default_  выполняем `mkcert example.test`
* Переименовать .pem файлы в fullchain.pem и privkey.pem
* В корневой директории проекта **docker-compose up -d**
* `sudo nano /etc/hosts` - добавить новый хост с названием проекта (pik.loc)

### Перезапуск api-gateway

```docker compose -f docker-compose.yaml up -d --no-deps --build api-gateway```